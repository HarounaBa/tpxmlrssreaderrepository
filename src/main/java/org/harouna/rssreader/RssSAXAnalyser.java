package org.harouna.rssreader;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RssSAXAnalyser {

	public static void main(String[] args) throws ClientProtocolException, IOException, ParserConfigurationException, SAXException {
		// TODO Auto-generated method stub
		String url = "http://weblogs.java.net/pub/q/weblogs_rss";
		
		HttpClient httpClient = HttpClientBuilder.create().build();
		
		HttpGet get = new HttpGet(url);
		
		HttpClientBuilder.create();
		
		HttpResponse response = httpClient.execute(get);
		
		StatusLine statusLine = response.getStatusLine();
		int statusCode = statusLine.getStatusCode();
		
		if(statusCode != HttpStatus.SC_OK){
			System.out.println("Request failed with code " + statusCode);
		}
		
		InputStream content = response.getEntity().getContent();
		
		DefaultHandler handler = new DefaultHandler(){
			public void startDocument(){
				System.out.println("D�but du document");
			}	
			
			
		};
		
		SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
		parser.parse(content, handler);
	}
}
