package org.harouna.tpxml.serie1Exercice4;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
//import org.harouna.tpxml.serie1Exercice3.HandlerCountNumberElementsItem;
import org.harouna.tpxml.serie1Exercice3.HandlerCountNumberSubElements;
/*import org.harouna.tpxml.serie1Exercice3.HandlerListTitlesElementItemTopic;
import org.harouna.tpxml.serie1Exercice3.HandlerListTitlesElementsItem; */
import org.harouna.tpxml.serie1Exercice3.HandlerShowRootElementName;
//import org.harouna.tpxml.serie1Exercice3.RSSReader; 
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Main {

	public static void main(String[] args) throws DocumentException, ParserConfigurationException, SAXException, IOException {
		// TODO Auto-generated method stub
		File file = new File("files/AWSECommerceService.wsdl.xml");
		FileReader fileReader = new FileReader();	
		InputStream content1 = fileReader.read(file);

		SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

		//Root Element's name ---> Question 2)
		DefaultHandler handler1 = new HandlerShowRootElementName();
		parser.parse(content1, handler1);

		//Number of sub-element in  types/schema ---> Question 4a)
		InputStream content2 = fileReader.read(file);
		DefaultHandler	handler2 = new Handler_CountNumberOfSubElements_In_Types_Schema();
		parser.parse(content2, handler2);
		
		//List of values of attributes name in types/schema --->Question 4b)
		InputStream content3 = fileReader.read(file);
		DefaultHandler handler3 = new HandlerListOfValuesAttributesName();
		parser.parse(content3, handler3);
		
		//List of values of attributes names in the elements message --> Question 5)
		InputStream content4 = fileReader.read(file);
		DefaultHandler handler4 = new HandlerListOfValuesAttributesName_ElementMessage();
		parser.parse(content4, handler4);
		
		//Operation name --> Question 6)
		InputStream content5 = fileReader.read(file);
		DefaultHandler handler5 = new HandlerOperationName("tns:CartGetRequestMsg");
		parser.parse(content5, handler5);

	}

}
