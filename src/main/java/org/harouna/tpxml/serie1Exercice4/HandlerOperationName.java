package org.harouna.tpxml.serie1Exercice4;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class HandlerOperationName extends DefaultHandler{

	boolean operation;
	public String messageName;
	String operationName;
	String operationName2;

	public HandlerOperationName (String messageName) {
		// TODO Auto-generated constructor stub
		this.messageName=messageName;

	}

	public void startElement(String uri, String localName, String qName, Attributes attributes)
	{
		if(qName.equals("operation"))
		{
			operation=true;

			operationName=attributes.getValue("name");
		}
		if(operation==true)
			if(qName.equals("output") || qName.equals("input"))
			{
				if(attributes.getValue("message")!=null)
				{
					if( attributes.getValue("message").equals(messageName))

						operationName2=operationName;
				}
			}
	}

	public void endElement(String uri, String localName, String qName)
	{

		if(qName.equals("operation")){
			operation=false;
		}
	}

	public void endDocument()
	{
		System.out.println("Le nom de l'operation qui utilise le message " + messageName + " est : " + operationName2 );			
		System.out.println("Fin du document ------------------------------------------------------");
	}

}
