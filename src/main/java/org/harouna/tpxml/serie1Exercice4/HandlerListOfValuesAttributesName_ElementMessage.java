package org.harouna.tpxml.serie1Exercice4;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class HandlerListOfValuesAttributesName_ElementMessage extends DefaultHandler{

	List<String> listValuesAttributesName_Message =new ArrayList<String>();

	public void startDocument()
	{
		System.out.println("Debut du document");
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes)
	{		
		if(qName.equals("message"))
			listValuesAttributesName_Message.add(attributes.getValue("name"));	
	}

	public void endDocument()
	{
		System.out.println("La liste des valeurs des attributs name des elements message est: ");			

		for(String s : listValuesAttributesName_Message){
			System.out.println(s);
		}
		System.out.println("Fin du document---------------------------------------------");
	}
}
