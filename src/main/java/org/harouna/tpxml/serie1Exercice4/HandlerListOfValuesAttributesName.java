package org.harouna.tpxml.serie1Exercice4;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

//Question 4b)
public class HandlerListOfValuesAttributesName extends DefaultHandler{

	boolean inTypes = false;
	boolean inSchema = false;
	int numberSubElements=0;
	int profondeur=0;

	List<String> listValuesAttributesName =new ArrayList<String>();
	public void startDocument(){

		System.out.println("D�but du document");
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes)
	{

		if(qName.equals("types"))
			inTypes=true;
		if(inTypes==true)
		{
			if(qName.equals("xs:schema"))
				inSchema=true;
		}

		if(inSchema==true)
		{	
			profondeur++;
			if(profondeur==2)
				listValuesAttributesName.add(attributes.getValue("name"));
		}
	}

	public void endElement(String uri, String localName, String qName)
	{

		if(inSchema==true)
			profondeur--;

		if(qName.equals("types"))
			inTypes=false;

		if(qName.equals("xs:schema"))
			inSchema=false;
	}


	public void endDocument()
	{
		System.out.println("La liste des valeurs des attributs name des sous elements types/schema : "  );			

		for(String s : listValuesAttributesName){
			System.out.println(s);			
		}
		System.out.println("Fin du document---------------------------------------------");			

	}
}
