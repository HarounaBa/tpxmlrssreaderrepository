package org.harouna.tpxml.serie1Exercice4;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
//Handler Question  4 a) 
public class Handler_CountNumberOfSubElements_In_Types_Schema  extends  DefaultHandler{

	int profondeur =0;
	int compteur =0;
	//int compteur2 =0;
	//int nombreItems = 0;

	boolean inTypes = false;
	boolean inSchema = false;
	
	public void startDocument(){

		System.out.println("D�but du document");
	}


	public void startElement(String uri, String localName, String qName, Attributes attributes){
		
		if(qName.equals("types") ){

			inTypes = true;
		}
		
		if(inTypes == true){			
			if(qName.equals("xs:schema")){						
				inSchema = true;	
			}			
		}		
		if(inSchema == true){
			profondeur++;
			if(profondeur == 2){
				compteur++;
			}
		}

		
	}

	public void endElement(String uri, String localName, String qName){

		if(inSchema == true){
			profondeur--;
		}
		
		if(qName.equals("types")){
			inTypes = false;
		}
		
		if(qName.equals("xs:schema")){
			inSchema = false;
		}
	}

	public void endDocument(){
		System.out.println("Nombre sous �l�ments : " + compteur);
		System.out.println("Fin du document\n------------------------------------------------");
	}	
}























