package org.harouna.tpxml.serie1Exercice3;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.dom4j.Document;

public class RSSReader {

	public InputStream reader(String url) throws ClientProtocolException, IOException{

		HttpClient httpClient = HttpClientBuilder.create().build();

		HttpGet get = new HttpGet(url);

		HttpClientBuilder.create();
		HttpResponse response = httpClient.execute(get);
		StatusLine statusLine = response.getStatusLine();
		int statusCode = statusLine.getStatusCode();

		if(statusCode != HttpStatus.SC_OK){
			System.out.println("Request failed with code " + statusCode);
		}
		InputStream content = response.getEntity().getContent();
		return content;
	}

	
}
