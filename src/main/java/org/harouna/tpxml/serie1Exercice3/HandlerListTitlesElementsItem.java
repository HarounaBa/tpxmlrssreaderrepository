package org.harouna.tpxml.serie1Exercice3;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;

import org.xml.sax.helpers.DefaultHandler;
//Handler Question 5
public class HandlerListTitlesElementsItem extends DefaultHandler {
	public void startDocument(){

		System.out.println("D�but du document");
	}

	int profondeur =0;
	int compteur1 =0;
	int compteur2 =0;
	int nombreItems = 0;
	String sousElement = null;
	
	List<String> listElements = new ArrayList<>(); 

	boolean title = false;
	String stringQName;

	public void startElement(String uri, String localName, String qName, Attributes attributes){
		stringQName = qName;
		if(profondeur ==1){
			compteur2++;
		}
		if (qName.equals("item")) {
			++nombreItems;
			title = true;
		}

		profondeur++; 
		if(sousElement == null){
			
			sousElement = qName;
			compteur1++;
		}								
	}

	public void endElement(String uri, String localName, String qName){
		profondeur--;
		if(sousElement.equals(qName)){
			sousElement = null;
		}

	}


	public void characters(char [] ch, int start, int length){
		String s = new String(ch, start, length);

		if( (stringQName !=null) && (this.title==true) && (stringQName.equals("title")) ){
			//System.out.println("title = " + string);
			listElements.add(s);
		}
	}

	public void endDocument(){
		System.out.println(listElements);
		System.out.println("Fin du document");
	}
}
