package org.harouna.tpxml.serie1Exercice3;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

//Handler Question 6
public class HandlerListTitlesElementItemTopic extends DefaultHandler {
	public void startDocument(){
		System.out.println("D�but du document");
	}

	int profondeur = 0;
	int compteur1 = 0;
	int compteur2 = 0;
	int nombreItems = 0;
	int nombreCategories = 0;
	
	int nombreAttributes;
	String stringQName;
	
	String sousElement = null;
	String selectedTitles;

	List<String> listElements = new ArrayList<>(); 
	
	boolean title = false;
	
	public void startElement(String uri, String localName, String qName, Attributes attributes){
		stringQName = qName;
		if(profondeur ==1){
			compteur2++;
		}
		if (qName.equals("item")) {
			++nombreItems;
			title = true;
		}

		profondeur++; 
		if(sousElement == null){
		
			sousElement = qName;
			compteur1++;
		}	

		for(int i = 0 ; i < nombreAttributes ; i++){
			if(title=true && attributes.getValue(i)
					.equals("http://www.java.net/topic/")){
				nombreCategories++;
			}
		}
	}

	public void endElement(String uri, String localName, String qName){
		profondeur--;

		if(sousElement.equals(qName)) {
			sousElement = null;
		}

		if(qName == "item"){
			if(nombreCategories !=0){
				listElements.add(selectedTitles);
			}
			nombreCategories = 0;
			title = false;
		}
	}

	public void characters(char [] ch, int start, int length){
		String s = new String(ch, start, length);
		if( (stringQName !=null) && (this.title==true) && (stringQName.equals("title")) ){

			listElements.add(s);
			selectedTitles = s;
			stringQName = null;
		}
	}

	public void endDocument(){
		System.out.println(listElements);
		System.out.println("Fin du document");
	}
}
