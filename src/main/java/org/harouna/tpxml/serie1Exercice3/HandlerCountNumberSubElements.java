package org.harouna.tpxml.serie1Exercice3;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
//Handler Question  3
public class HandlerCountNumberSubElements extends DefaultHandler{

	public void startDocument(){

		System.out.println("D�but de document");
	}

	int profondeur =0;
	int compteur1 =0;
	int compteur2 =0;
	int nombreItems = 0;
	String sousElement = null;

	public void startElement(String uri, String localName, String qName, Attributes attributes){
		if(profondeur ==1){
			compteur2++;
		}

		profondeur++;
		
		if(sousElement == null){
		
	     	sousElement = qName;
			compteur1++;
		}								
	}

	public void endElement(String uri, String localName, String qName){
		profondeur--;
		if(sousElement.equals(qName)){
			sousElement = null;
		}

	}

	public void endDocument(){

		System.out.println("Nombre sous �l�ments : " + compteur1);
		System.out.println("Fin du document");
	}
}
