package org.harouna.tpxml.serie1Exercice3;


import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Main {

	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
		// TODO Auto-generated method stub
		String url = "http://weblogs.java.net/pub/q/weblogs_rss";
		RSSReader rssReader = new RSSReader();
		
		InputStream content1 = rssReader.reader(url);
		InputStream content2 = rssReader.reader(url);
		InputStream content3 = rssReader.reader(url);
		InputStream content4 = rssReader.reader(url);
		InputStream content5 = rssReader.reader(url);

		DefaultHandler handler1 = new HandlerShowRootElementName();
		DefaultHandler handler2 = new HandlerCountNumberSubElements();
		DefaultHandler handler3 = new HandlerCountNumberElementsItem();
		DefaultHandler handler4 = new HandlerListTitlesElementsItem();
		DefaultHandler handler5 = new HandlerListTitlesElementItemTopic();
		
		SAXParser parser1 = SAXParserFactory.newInstance().newSAXParser();
		SAXParser parser2 = SAXParserFactory.newInstance().newSAXParser();
		SAXParser parser3 = SAXParserFactory.newInstance().newSAXParser();
		SAXParser parser4 = SAXParserFactory.newInstance().newSAXParser();
		SAXParser parser5 = SAXParserFactory.newInstance().newSAXParser();

		//parser1.parse(content1, handler1);
		parser2.parse(content2, handler2);

	}
}

