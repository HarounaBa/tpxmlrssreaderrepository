package org.harouna.tpxml.serie1Exercice4_XPath;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class XPath_Question5 {

	public List<String> listOfValuesAttributeName_ElementMessage() throws DocumentException
	{

		List<String> listValuesAttributeName_ElementMessage =new ArrayList<String>();

		File file= new File("files/AWSECommerceService.wsdl.xml");		
		SAXReader sax = new SAXReader ();
		Document doc = sax.read(file);

		List<Element> listSubElements= doc.selectNodes("//*[name()='message']");

		for(Element message : listSubElements)
		{
			listValuesAttributeName_ElementMessage.add(message.attribute("name").getValue());
		}

		return listValuesAttributeName_ElementMessage;
	}

}
