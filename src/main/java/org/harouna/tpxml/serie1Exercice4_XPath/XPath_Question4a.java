package org.harouna.tpxml.serie1Exercice4_XPath;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.jdom2.JDOMException;


public class XPath_Question4a {
	//M�thode qui permet d'afficher l'�lement racine
	public int numberOfSubElements() throws DocumentException{
		
		File file= new File("files/AWSECommerceService.wsdl.xml");		
		SAXReader sax=new SAXReader();
		Document doc=sax.read(file);
		
		List<Element> listSubElements= doc.selectNodes("//*[name()='types']/*[name()='xs:schema']/*");
		
		return listSubElements.size();
        
	}
}
