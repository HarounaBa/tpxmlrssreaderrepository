package org.harouna.tpxml.serie1Exercice4_XPath;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class XPath_Question6 {
	
	public String operationName(String messageName) throws DocumentException
	{
		File file= new File("files/AWSECommerceService.wsdl.xml");		
		SAXReader sax = new SAXReader ();
		Document doc = sax.read(file);
		
		StringBuilder concat=new StringBuilder();
		concat.append("'").append(messageName).append("']");
		
		Element element= (Element) doc.selectSingleNode("//*[name()='operation']/*[@message=" + concat.toString());
		
		 return element.getParent().attribute("name").getValue();
	}
}
