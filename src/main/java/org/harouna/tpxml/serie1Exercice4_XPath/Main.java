package org.harouna.tpxml.serie1Exercice4_XPath;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import org.dom4j.DocumentException;
import org.harouna.tpxml.serie1Exercice4.FileReader;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, DocumentException {
		// TODO Auto-generated method stub

		//Question 4a)
		XPath_Question4a question4a =new XPath_Question4a();

		System.out.println("Le nombre de sous elements pr�sent dans l'�l�ment types/schema :"  + question4a.numberOfSubElements());
		System.out.println("\n----------------------------------------------------\n");
		
		//Question 4b)
		XPath_Question4b question4b =new XPath_Question4b();
		System.out.println("La liste des valeurs des attributs name des sous elements de types/schema : ");
		List<String> listOfValues= question4b.listOfValuesAttributesName();
		
		for(String value : listOfValues)
		{
			System.out.println(value);
		}
		System.out.println("\n----------------------------------------------------\n");
		
		//Question 5)
		XPath_Question5 question5 =new XPath_Question5();
		System.out.println("La liste des valeurs des attributs name des elements message est: ");			
		List<String> listOfValues_ElementMessage= question5.listOfValuesAttributeName_ElementMessage();

		for(String value : listOfValues_ElementMessage)
		{
			System.out.println(value);
		}
		System.out.println("\n----------------------------------------------------\n");
		
		
		//Question 6
		XPath_Question6 question6 =new XPath_Question6();
		String message = "tns:CartGetRequestMsg";
		System.out.println("Le nom de l'operation qui utilise le message "+ message +" : " + question6.operationName(message));			

	}
}
